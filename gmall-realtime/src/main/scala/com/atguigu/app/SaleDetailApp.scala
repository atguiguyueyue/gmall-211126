package com.atguigu.app

import java.util

import com.alibaba.fastjson.JSON
import com.atguigu.bean.{OrderDetail, OrderInfo, SaleDetail, UserInfo}
import com.atguigu.constants.GmallConstants
import com.atguigu.util.{MyEsUtil, MyKafkaUtil}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import redis.clients.jedis.Jedis

import collection.JavaConverters._
import org.json4s.native.Serialization

object SaleDetailApp {
  def main(args: Array[String]): Unit = {
    //1.创建Sparkconf
    val sparkConf: SparkConf = new SparkConf().setAppName("SaleDetailApp").setMaster("local[*]")

    //2.创建StreamingContext
    val ssc: StreamingContext = new StreamingContext(sparkConf, Seconds(5))

    //3.消费kafka数据
    val orderInfoKafkaDStream: InputDStream[ConsumerRecord[String, String]] = MyKafkaUtil.getKafkaStream(GmallConstants.KAFKA_TOPIC_ORDER, ssc)

    val orderDetailKafkaDStream: InputDStream[ConsumerRecord[String, String]] = MyKafkaUtil.getKafkaStream(GmallConstants.KAFKA_TOPIC_ORDER_DETAIL, ssc)

    //4.将订单表的数据转成样例类
    val orderInfoDStream = orderInfoKafkaDStream.mapPartitions(partition => {
      partition.map(record => {
        val orderInfo: OrderInfo = JSON.parseObject(record.value(), classOf[OrderInfo])

        //补全create_data&create_hour
        orderInfo.create_date = orderInfo.create_time.split(" ")(0)
        orderInfo.create_hour = orderInfo.create_time.split(" ")(1).split(":")(0)
        //对手机号脱敏
        orderInfo.consignee_tel = orderInfo.consignee_tel.substring(0, 3) + "********"
        (orderInfo.id,orderInfo)
      })
    })

    //将订单明细表的数据转成样例类
    val orderDetailDStream = orderDetailKafkaDStream.mapPartitions(partition => {
      partition.map(record => {
        val orderDetail: OrderDetail = JSON.parseObject(record.value(), classOf[OrderDetail])
        (orderDetail.order_id,orderDetail)
      })
    })

    //5.join订单表数据和订单明细表数据
//    val joinDStream: DStream[(String, (OrderInfo, OrderDetail))] = orderInfoDStream.join(orderDetailDStream)
    val fullOuterJoinDStream: DStream[(String, (Option[OrderInfo], Option[OrderDetail]))] = orderInfoDStream.fullOuterJoin(orderDetailDStream)

    //6.采用加缓存的方式解决因网络延迟所带来的数据丢失问题
    val noUserSaleDetailDStream: DStream[SaleDetail] = fullOuterJoinDStream.mapPartitions(partition => {
      implicit val formats = org.json4s.DefaultFormats
      //创建List集合用来存放结果数据（SaleDetail）
      val details: util.ArrayList[SaleDetail] = new util.ArrayList[SaleDetail]()
      //获取Redis连接
      val jedis: Jedis = new Jedis("hadoop102", 6379)

      partition.foreach { case (orderId, (infoOpt, detailOpt)) =>
        //orderInfo RedisKey
        val orderInfoRedisKey: String = "orderInfo:" + orderId
        //orderDetail RedisKey
        val orderDetailRedisKey: String = "orderDetail:" + orderId

        //a.判断订单表的数据是否存在
        if (infoOpt.isDefined) {
          //订单存在
          //a.1获取订单表数据
          val orderInfo: OrderInfo = infoOpt.get
          //a.2判断订单明细表数据是否存在
          if (detailOpt.isDefined) {
            //a.3订单明细表存在，则取出数据
            val orderDetail: OrderDetail = detailOpt.get
            //a.4将两个表的数据组合成样例类
            val detail: SaleDetail = new SaleDetail(orderInfo, orderDetail)
            details.add(detail)
          }
          //b.将orderInfo数据存入Redis
          //不能用toJSONString方法将样例类转为Json字符串，会编译报错
          //JSON.toJSONString(orderInfo)
          val orderInfoJson: String = Serialization.write(orderInfo)
          //b.1将Json字符串存入Redis
          jedis.set(orderInfoRedisKey, orderInfoJson)
          //b.2给存入redis中的数据设置过期时间
          jedis.expire(orderInfoRedisKey, 30)

          //c.查询orderDetail数据
          //先判断orderDetailRedisKey是否存在
          if (jedis.exists(orderDetailRedisKey)) {
            //orderDetail数据存在
            val detailSet: util.Set[String] = jedis.smembers(orderDetailRedisKey)
            for (elem <- detailSet.asScala) {
              //将查询出来的Json字符串转为样例类
              val orderDetail: OrderDetail = JSON.parseObject(elem, classOf[OrderDetail])
              val detail: SaleDetail = new SaleDetail(orderInfo, orderDetail)
              details.add(detail)
            }
          }
        } else {
          //orderInfo数据不在
          //d.判断orderDetail数据是否存在
          if (detailOpt.isDefined) {
            //orderDetail数据存在
            //取出orderDetail数据
            val orderDetail: OrderDetail = detailOpt.get
            //e.查询orderInfo缓存中是否能有关联上的数据
            if (jedis.exists(orderInfoRedisKey)) {
              //有能够与orderDetail关联上的数据
              val infoStr: String = jedis.get(orderInfoRedisKey)
              //将查询出来的json字符串转为样例类
              val orderInfo: OrderInfo = JSON.parseObject(infoStr, classOf[OrderInfo])
              val detail: SaleDetail = new SaleDetail(orderInfo, orderDetail)
              details.add(detail)
            } else {
              //对方缓存中没有能join上的数据
              //f.将自己存入缓存，等待orderInfo数据到来进行关联
              val orderDetailJsonStr: String = Serialization.write(orderDetail)
              jedis.sadd(orderDetailRedisKey, orderDetailJsonStr)
              //对orderDetail数据设置过期时间
              jedis.expire(orderDetailRedisKey, 30)
            }
          }
        }
      }
      jedis.close()
      details.asScala.toIterator
    })

    //7.关联UserInfo数据
    val saleDetailDStream: DStream[SaleDetail] = noUserSaleDetailDStream.mapPartitions(partition => {
      //获取redis连接
      val jedis: Jedis = new Jedis("hadoop102", 6379)
      val details: Iterator[SaleDetail] = partition.map(saleDetail => {
        //查询redis中userInfo的数据
        val userInfoRedisKey: String = "userInfo:" + saleDetail.user_id
        val userInfoJsonStr: String = jedis.get(userInfoRedisKey)
        //将查询出来的json字符串数据转为样例类
        val userInfo: UserInfo = JSON.parseObject(userInfoJsonStr, classOf[UserInfo])
        //关联用户数据
        saleDetail.mergeUserInfo(userInfo)
        saleDetail
      })
      jedis.close()
      details
    })
    saleDetailDStream.print()

    //8.将数据写入ES
    saleDetailDStream.foreachRDD(rdd=>{
      rdd.foreachPartition(partition=>{
        val list: List[(String, SaleDetail)] = partition.toList.map(saleDetail => {
          (saleDetail.order_detail_id, saleDetail)
        })
        //调用ES工具类将数据存入ES
        MyEsUtil.insertBulk(GmallConstants.ES_SALEDETAIL+"211126",list)
      })
    })

    ssc.start()
    ssc.awaitTermination()
  }

}
