package com.atguigu.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@org.springframework.stereotype.Controller
//@RestController=@Controller+@ResponseBody
@RestController
public class Controller {
    @RequestMapping("test1")
//    @ResponseBody
    public String test(){
        System.out.println("123");
        return "success";
    }

    @RequestMapping("test2")
    public String test1(
            @RequestParam("name") String na,
            @RequestParam("age") Integer a){
        System.out.println("2222");
        return "name:" + na+ "   age:" + a;
    }
}
