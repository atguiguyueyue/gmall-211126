package com.atguigu.read;

import com.sun.xml.internal.bind.v2.TODO;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.MaxAggregation;
import io.searchbox.core.search.aggregation.MetricAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.max.MaxAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ES04_Read_Code {
    public static void main(String[] args) throws IOException {
        //1.创建客户端工厂
        JestClientFactory jestClientFactory = new JestClientFactory();

        //2.设置连接属性
        jestClientFactory.setHttpClientConfig(new HttpClientConfig.Builder("http://hadoop102:9200").build());

        //3.获取连接
        JestClient jestClient = jestClientFactory.getObject();

        //用代码的方式代替字符串
        //TODO ---------------------------{}--------------------------------
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //TODO ---------------------------bool--------------------------------
        BoolQueryBuilder bool = new BoolQueryBuilder();

        //TODO ---------------------------term--------------------------------
        TermQueryBuilder term = new TermQueryBuilder("sex", "男");

        //TODO ---------------------------filter--------------------------------
        bool.filter(term);

        //TODO ---------------------------match--------------------------------
        MatchQueryBuilder match = new MatchQueryBuilder("favo", "羽毛球");

        //TODO ---------------------------must--------------------------------
        bool.must(match);

        //TODO ---------------------------query--------------------------------
        searchSourceBuilder.query(bool);

        //TODO ---------------------------terms--------------------------------
        TermsAggregationBuilder terms = AggregationBuilders.terms("groupByClass").field("class_id");

        //TODO ---------------------------max--------------------------------
        MaxAggregationBuilder max = AggregationBuilders.max("groupByAge").field("age");

        //TODO ---------------------------aggs--------------------------------
        searchSourceBuilder.aggregation(terms.subAggregation(max));

        //TODO ---------------------------from--------------------------------
        searchSourceBuilder.from(0);

        //TODO ---------------------------size--------------------------------
        searchSourceBuilder.size(2);

        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex("student")
                .addType("_doc")
                .build();

        SearchResult searchResult = jestClient.execute(search);

        //TODO 获取命中数据条数
        System.out.println("total:"+searchResult.getTotal());

        //TODO 获取明细数据
        List<SearchResult.Hit<Map, Void>> hits = searchResult.getHits(Map.class);
        for (SearchResult.Hit<Map, Void> hit : hits) {
            System.out.println("_index:"+hit.index);
            System.out.println("_type:"+hit.type);
            System.out.println("_id:"+hit.id);
            System.out.println("_score:"+hit.score);
            Map source = hit.source;
            for (Object o : source.keySet()) {
                System.out.println(o+":"+source.get(o));
            }
        }

        //TODO 获取聚合组数据
        MetricAggregation aggregations = searchResult.getAggregations();

        //获取班级聚合组数据
        TermsAggregation groupByClass = aggregations.getTermsAggregation("groupByClass");
        List<TermsAggregation.Entry> buckets = groupByClass.getBuckets();
        for (TermsAggregation.Entry bucket : buckets) {
            System.out.println("key:"+bucket.getKey());
            System.out.println("doc_count:"+bucket.getCount());

            //获取年龄聚合组数据
            MaxAggregation groupByAge = bucket.getMaxAggregation("groupByAge");
            System.out.println("value:"+groupByAge.getMax());
        }


        //关闭连接
        jestClient.shutdownClient();
    }
}
