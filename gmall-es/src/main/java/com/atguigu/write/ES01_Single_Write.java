package com.atguigu.write;

import com.atguigu.bean.Movie;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Index;

import java.io.IOException;
import java.util.HashMap;

public class ES01_Single_Write {
    public static void main(String[] args) throws IOException {
        //1.创建客户端工厂
        JestClientFactory jestClientFactory = new JestClientFactory();

        //2.设置连接属性
        jestClientFactory.setHttpClientConfig(new HttpClientConfig.Builder("http://hadoop102:9200").build());

        //3.通过客户端工厂获取连接
        JestClient jestClient = jestClientFactory.getObject();

        //利用map写入数据
        HashMap<String, String> map = new HashMap<>();
        map.put("id", "103");
        map.put("name", "新蝙蝠侠");
        Index index = new Index.Builder(map)
                .id("1003")
                .type("_doc")
                .index("movie")
                .build();

        //利用javabean来写入数据
//        Movie movie2 = new Movie("102", "金刚大战哥斯拉");
//        Index index = new Index.Builder(movie2)
//                .id("1002")
//                .type("_doc")
//                .index("movie")
//                .build();

     /*   Index index = new Index.Builder("{\n" +
                "  \"id\":\"101\",\n" +
                "  \"name\":\"蜘蛛侠2：英雄远征\"\n" +
                "}")
                .index("movie")
                .type("_doc")
                .id("1001")
                .build();*/
        jestClient.execute(index);


        //关闭连接
        jestClient.shutdownClient();
    }
}
