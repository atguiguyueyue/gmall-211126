package com.atguigu.bean;

import io.searchbox.action.SingleResultAbstractDocumentTargetedAction;
import io.searchbox.core.Index;

import java.util.zip.ZipInputStream;

public class Student2 {
    private String name;
    private String age;
    private String sex;

    protected Student2(Builder builder) {
        this.name = "zs";
    }

    @Override
    public String toString() {
        return "Student2{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }

    public static class Builder{

        private String name;
        private String age;
        private String sex;

        public Builder() {
        }

        public Student2 build() {
            return new Student2(this);
        }
        public Builder name(String name) {
            this.name = name;
            return  this;
        }

        public Builder age(String age) {
            this.age = age;
            return this;
        }

        public Builder sex(String sex) {
            this.sex = sex;
            return this;
        }
    }
}
