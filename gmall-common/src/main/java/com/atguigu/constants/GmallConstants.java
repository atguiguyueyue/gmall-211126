package com.atguigu.constants;

public class GmallConstants {
    //启动日志Topic
    public static final String KAFKA_TOPIC_STARTUP = "GMALL_STARTUP";

    //事件日志Topic
    public static final String KAFKA_TOPIC_EVENT = "GMALL_EVENT";

    //订单表主题
    public static final String KAFKA_TOPIC_ORDER = "GMALL_ORDER";

    //预警需求ES前缀
    public static final String ES_ALERT = "gmall_coupon_alert";

    //订单明细表主题
    public static final String KAFKA_TOPIC_ORDER_DETAIL = "TOPIC_ORDER_DETAIL";

    //用户表主题
    public static final String KAFKA_TOPIC_USER = "TOPIC_USER_INFO";

    //灵活分析ES前缀
    public static final String ES_SALEDETAIL = "gmall2021_sale_detail";
}
