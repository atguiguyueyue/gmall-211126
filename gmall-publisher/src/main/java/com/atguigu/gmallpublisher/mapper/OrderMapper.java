package com.atguigu.gmallpublisher.mapper;

import java.util.List;
import java.util.Map;

public interface OrderMapper {
    //交易额总数抽象方法
    public Double selectOrderAmountTotal(String date);

    //交易额分时数据抽象方法
    public List<Map> selectOrderAmountHourMap(String date);
}
